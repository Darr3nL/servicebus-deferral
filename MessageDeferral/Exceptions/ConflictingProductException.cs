﻿using System;

namespace MessageDeferral.Exceptions
{
    public class ConflictingProductException : Exception
    {
        public ConflictingProductException(int id) : base($"Prodct with the id `{id}` already exists")
        {
        }
    }
}
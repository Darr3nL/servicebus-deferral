﻿using System;

namespace MessageDeferral.Exceptions
{
    public class ProductNotFoundException : Exception
    {
        public ProductNotFoundException(int id) : base($"Product with the id `{id}` was not found")
        {
        }
    }
}
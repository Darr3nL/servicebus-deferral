﻿namespace MessageDeferral
{
    public class Product
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
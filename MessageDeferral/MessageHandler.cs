﻿using MessageDeferral.Exceptions;
using MessageDeferral.Messages;
using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MessageDeferral
{
    public class MessageHandler
    {
        private readonly MessageReceiver _createdMessageReceiver;
        private readonly MessageReceiver _updatedMessageReceiver;
        private readonly MessageReceiver _updatedDeferralMessageReceiver;

        public readonly MessageSender DeferredMessageSender;
        public readonly MessageSender CreatedMessageSender;
        public readonly MessageSender UpdatedMessageSender;

        public readonly ProductRepository Repository;

        public MessageHandler(ServiceBusOptions options)
        {
            Repository = new ProductRepository();

            // set up the message receivers and senders
            var createdEntityName = EntityNameHelper.FormatSubscriptionPath(options.ProductCreatedTopicName, options.SubscriptionName);
            _createdMessageReceiver = new MessageReceiver(options.ConnectionString, createdEntityName);
            _createdMessageReceiver.RegisterMessageHandler(ReceiveCreatedMessage, DefaultMessageHandlerOptions());

            CreatedMessageSender = new MessageSender(options.ConnectionString, options.ProductCreatedTopicName);

            var updatedEntityName = EntityNameHelper.FormatSubscriptionPath(options.ProductUpdatedTopicName, options.SubscriptionName);
            _updatedMessageReceiver = new MessageReceiver(options.ConnectionString, updatedEntityName);
            _updatedMessageReceiver.RegisterMessageHandler(ReceiveUpdatedMessage, DefaultMessageHandlerOptions());

            UpdatedMessageSender = new MessageSender(options.ConnectionString, options.ProductUpdatedTopicName);

            var updatedDeferredEntityName = EntityNameHelper.FormatSubscriptionPath(options.ProductUpdatedDeferralTopicName, options.SubscriptionName);
            _updatedDeferralMessageReceiver = new MessageReceiver(options.ConnectionString, updatedDeferredEntityName);
            _updatedDeferralMessageReceiver.RegisterMessageHandler(ReceiveUpdatedDeferredMessage, DefaultMessageHandlerOptions());

            DeferredMessageSender = new MessageSender(options.ConnectionString, options.ProductUpdatedDeferralTopicName);
        }

        /// <summary>
        /// When we receive a ProductCreated message we will attempt to add it to the repository.
        /// If the attempt fails the message will be dead lettered, otherwise the message will be completed
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task ReceiveCreatedMessage(Message message, CancellationToken cancellationToken)
        {
            Console.WriteLine("Received product created message");
            var body = Encoding.UTF8.GetString(message.Body);
            var deserializedBody = JsonConvert.DeserializeObject<ProductCreated>(body);

            var product = new Product
            {
                Id = deserializedBody.Id,
                Status = deserializedBody.Status
            };

            try
            {
                Repository.Add(product);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                await _createdMessageReceiver.DeadLetterAsync(message.SystemProperties.LockToken, e.Message);
            }

            await _createdMessageReceiver.CompleteAsync(message.SystemProperties.LockToken);
        }

        /// <summary>
        /// When we receive the deferral message, we attempt to receive the deferred ProductCreated message from service bus
        /// by its sequence number. Once we have received this message successfully we handle it using the ReceiveUpdatedMessage method.
        /// If this is successful, we can complete the deferral message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task ReceiveUpdatedDeferredMessage(Message message, CancellationToken cancellationToken)
        {
            Console.WriteLine("Received deferral message");
            var body = Encoding.UTF8.GetString(message.Body);
            var deserializedBody = JsonConvert.DeserializeObject<DeferralMessage>(body);

            var updatedMessage = await _updatedMessageReceiver.ReceiveDeferredMessageAsync(deserializedBody.SequenceNumber);
            await ReceiveUpdatedMessage(updatedMessage, cancellationToken);
            
            await _updatedDeferralMessageReceiver.CompleteAsync(message.SystemProperties.LockToken);
        }

        /// <summary>
        /// When we receive a ProductUpdated we will attempt to update the existing product in the repository.
        /// If the repository throws a ProductNotFoundException, we will defer this message until a later time by
        /// placing a new message on a separate topic which is scheduled to be delivered in a few moments.
        /// Otherwise the message will be dead lettered if any other exception is thrown or completed if the action is successful.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task ReceiveUpdatedMessage(Message message, CancellationToken cancellationToken)
        {
            Console.WriteLine("Received product updated message");
            var body = Encoding.UTF8.GetString(message.Body);
            var deserializedBody = JsonConvert.DeserializeObject<ProductUpdated>(body);

            var product = new Product
            {
                Id = deserializedBody.Id,
                Status = deserializedBody.Status
            };

            try
            {
                Repository.Update(product);
            }
            catch (ProductNotFoundException e)
            {
                Console.WriteLine("Product not found");
                // place a scheduled message on a separate topic which will be retrieved by the createdMessageDeferredReceiver after the specified time out
                // then defer the original ProductUpdated message which will leave it in the active subscription but make it unavailable to be retrieved by
                // any method other than specifically requesting it
                var deferralMessage = new DeferralMessage
                {
                    SequenceNumber = message.SystemProperties.SequenceNumber
                };

                Console.WriteLine("Dispatching deferral message");
                await DeferredMessageSender.ScheduleMessageAsync(GetMessage(deferralMessage), DateTimeOffset.UtcNow.AddSeconds(2));
                await _updatedMessageReceiver.DeferAsync(message.SystemProperties.LockToken);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                await _updatedMessageReceiver.DeadLetterAsync(message.SystemProperties.LockToken, e.Message);
            }

            await _updatedMessageReceiver.CompleteAsync(message.SystemProperties.LockToken);
        }

        private MessageHandlerOptions DefaultMessageHandlerOptions()
        {
            var messageHandlerOptions = new MessageHandlerOptions(_ => Task.CompletedTask)
            {
                MaxConcurrentCalls = 1,
                AutoComplete = false
            };

            return messageHandlerOptions;
        }

        private Message GetMessage(object message)
        {
            var serialized = JsonConvert.SerializeObject(message);
            return new Message(Encoding.UTF8.GetBytes(serialized));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using MessageDeferral.Exceptions;

namespace MessageDeferral
{
    public class ProductRepository
    {
        public readonly List<Product> Products;

        public ProductRepository()
        {
            Products = new List<Product>();
        }

        public void Add(Product product)
        {
            var existingProduct = GetProductById(product);

            if (existingProduct is not null)
            {
                throw new ConflictingProductException(product.Id);
            }

            Products.Add(product);
            Console.WriteLine($"Created product with status {product.Status}");

        }

        public void Update(Product product)
        {
            var existingProduct = GetProductById(product);

            if (existingProduct is null)
            {
                throw new ProductNotFoundException(product.Id);
            }

            existingProduct.Status = product.Status;

            Console.WriteLine($"Updated product status to {product.Status}");
        }

        private Product GetProductById(Product product)
        {
            var existingProduct = Products.FirstOrDefault(p => p.Id == product.Id);
            return existingProduct;
        }
    }
}
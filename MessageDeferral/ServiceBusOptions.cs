﻿namespace MessageDeferral
{
    public class ServiceBusOptions
    {
        public string ConnectionString { get; set; }
        public string ProductCreatedTopicName { get; set; }
        public string ProductUpdatedDeferralTopicName { get; set; }
        public string ProductUpdatedTopicName { get; set; }
        public string SubscriptionName { get; set; }
    }
}
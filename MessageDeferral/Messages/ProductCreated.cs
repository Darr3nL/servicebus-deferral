﻿namespace MessageDeferral.Messages
{
    public class ProductCreated
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
﻿namespace MessageDeferral.Messages
{
    public class DeferralMessage
    {
        public long SequenceNumber { get; set; }
    }
}
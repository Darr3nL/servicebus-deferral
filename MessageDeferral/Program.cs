﻿using MessageDeferral.Messages;
using Microsoft.Azure.ServiceBus;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MessageDeferral
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .Build();

            var options = new ServiceBusOptions();
            configuration.GetSection("ServiceBus").Bind(options);

            RunTestHarness(options);
        }

        private static async Task DispatchCreateProduct(MessageHandler messageHandler)
        {
            var createdMessage = new ProductCreated
            {
                Id = 1,
                Status = "Created"
            };

            await messageHandler.CreatedMessageSender.SendAsync(GetMessage(createdMessage));
        }

        private static async Task DispatchUpdateProduct(MessageHandler messageHandler)
        {
            var updateMessage = new ProductUpdated
            {
                Id = 1,
                Status = "Updated!"
            };

            await messageHandler.UpdatedMessageSender.SendAsync(GetMessage(updateMessage));
        }

        private static Message GetMessage(object message)
        {
            var serialized = JsonConvert.SerializeObject(message);
            return new Message(Encoding.UTF8.GetBytes(serialized));
        }
        private static void RunTestHarness(ServiceBusOptions options)
        {
            var messageHandler = new MessageHandler(options);

            var i = 0;

            while (true)
            {
                switch (i)
                {
                    case 4:
                        DispatchUpdateProduct(messageHandler).Wait();
                        break;

                    case 24:
                        DispatchCreateProduct(messageHandler).Wait();
                        break;

                    case 40:
                        return;
                }

                Thread.Sleep(250);

                i++;
            }
        }
    }
}